package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public ArrayList<FridgeItem> itemsInFridge;
    public int totalSize;

    public Fridge() {
        this.itemsInFridge = new ArrayList<FridgeItem>();
        this.totalSize = 20;
    }   

    public int nItemsInFridge() {
        return this.itemsInFridge.size();
    }

    public int totalSize() {
        return this.totalSize;
    }

    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            this.itemsInFridge.add(item);
            return true;
        }

        else {
            return false;
        }
    }

    public void takeOut(FridgeItem item) {
        if (itemsInFridge.contains(item)) {
            this.itemsInFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
        
        
    }

    public void emptyFridge() {
        this.itemsInFridge.clear();
    }

    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();

        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()) {
                //itemsInFridge.remove(item);
                expiredItems.add(item);
            }
        }

        itemsInFridge.removeAll(expiredItems);

        return expiredItems;
    }



}
